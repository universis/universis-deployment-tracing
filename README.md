# Universis deploy tracing

Stores and present information regarding the latest universis deploy

## Development

- Clone the repository
- Run `npm i`
- Copy the `.env.example` file to a `.env` file
- Fill the database value to a json file path (the directories must exist)
- Fill a token to be used as authentication strategy
- Run `npm start`
- Navigate to `localhost:9000`

Authentication Header: `x-universis-deploy-tracing-token`

## Production

- Clone the repository
- Run `npm i`
- Create the env variables for the DB and the token
- Create an env variable for `UNIVERSIS_DEPLOY_TRACING_PORT` (default is `9000`)
- Run `npm start:prod`


## Apidoc

You can use the `npm run doc` to create an apidoc containing information about how to POST data to this service.

## Docker setup

### Build

- (optional) Fill the  $TAG variable
- Run `docker build -t universis-deploy-tracing:${TAG} .`

### Test

```shell
docker run --rm -p 9000:9000 \
-e "UNIVERSIS_DEPLOY_TRACING_DB_PATH=/tmp/db.json" \
-e "UNIVERSIS_DEPLOY_TRACING_AUTH_TOKEN=1234" \
universis-deploy-tracing:$TAG`
```

### Docker-compose

- Run `REGISTRY="$REGISTRY" TAG="${TAG}" AUTH_TOKEN="$AUTH_TOKEN" UNIVERSIS_DEPLOY_TRACING_PORT=$PORT docker-compose up`

### Deploy

#### Production build

- Fill the ${REGISTRY} and ${TAG} variables
- Run `docker build -t ${REGISTRY}/universis-deploy-tracing:${TAG} .`
