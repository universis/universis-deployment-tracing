(function () {

  function getServiceURL(service) {
    const baseURL = 'https://gitlab.com/universis'
    switch(service){
      case 'api-universis': return `${baseURL}/universis-api-private`;
      case 'api-registrar': return `${baseURL}/universis-api-private`;
      case 'teachers': return `${baseURL}/universis-teachers`;
      case 'students': return `${baseURL}/universis-students`;
      case 'registrar': return `${baseURL}/universis`;
      case 'register':return `${baseURL}/universis-register`;
      case 'dining':return `${baseURL}/universis-dining`;
      case 'user-admin':return `${baseURL}/universis-admin`;
      case 'evaluation':return `${baseURL}/evaluation-app`;
      case 'robot':return `${baseURL}/robot`;
      case 'robot3':return `${baseURL}/robot3`;
      case 'qa-dashboard': return ``;
      case 'spm-dashboard': return ``;
    }
  }

  function generateTd(data) {
    const td = document.createElement('td');

    if (data) {
      td.innerHTML = data.sourceCommit;
      td.setAttribute('data-service', data.service);
      td.setAttribute('data-pipeline', data.pipeline);
      td.setAttribute('data-job', data.job);
      td.addEventListener('click', (event) => {showActionData(event)});
      td.setAttribute('class', 'clickable');
    } else {
      td.innerHTML = '-';
    }


    return td;
  }

  function populateDescriptionRow(logs) {
    const row = document.querySelector(`[data-service='description']`);

    if (row) {
      const descriptionTd = document.createElement('td');
      descriptionTd.innerHTML = '';
      row.appendChild(descriptionTd);

      // for each service,
      // find last record of db.json
      // show description field
      ['test01', 'test02', 'test05', 'test06', 'staging', 'production'].forEach((vps) => {
        const serviceData = logs[vps];

        // Get last log entry for this vps
        const tdData = serviceData ? Object.values(serviceData)[Object.values(serviceData).length - 1].description : ' ';

        const td = document.createElement('td');
        td.innerHTML = tdData || '';
        row.appendChild(td);
      });
    }
  }

  function populateServiceRow(service, serviceData) {
    const row = document.querySelector(`[data-service=${service}]`);
    if (row) {
      const titleTd = document.createElement('td');
      titleTd.innerHTML = service;
      row.appendChild(titleTd);

      ['test01', 'test02', 'test05', 'test06', 'staging', 'production'].forEach((vps) => {
        const vpsData = serviceData ? serviceData[vps] : undefined;

        const td = generateTd(vpsData);
        row.appendChild(td);
      });
    }
  }

  function createLiElement(name, linkText, link) {
    const li = document.createElement('li');
    li.innerText = `${name}: `;
    if (link && linkText) {
      const a = document.createElement('a');
      a.setAttribute('href', link);
      a.innerHTML = linkText;
      li.appendChild(a);
    } else if (!link && linkText) {
      li.innerText = `${name}: ${linkText}`;
    } else {
      li.innerText = `${name}: -`;
    }

    return li;
  }

  function showAttributeList(record) {
    document.querySelector('.js-selected-service-wrapper').removeAttribute('hidden');
    document.querySelector('.js-selected-service').innerHTML = `Selected Service: ${record.service}@${record.vps}`;
    document.querySelector('.js-subtitle').innerHTML = `${record.description}`;

    const list = document.querySelector('.js-selected-service-attributes');
    list.innerHTML = "";

    list.appendChild(createLiElement('VM', record.vps));
    list.appendChild(createLiElement('Service', record.service));
    list.appendChild(createLiElement('Source commit', `${record.sourceCommit} - ${record.sourceCommitTitle}`, `${getServiceURL(record.service)}/-/commit/${record.sourceCommit}`));
    list.appendChild(createLiElement('Source branch', record.sourceBranch, `${getServiceURL(record.service)}/-/tree/${record.sourceBranch}`));
    list.appendChild(createLiElement('Deploy commit', `${record.deployCommit} - ${record.deployCommitTitle}`, `https://repo.it.auth.gr/app/universis-deploy/-/commit/${record.deployCommit}`));
    list.appendChild(createLiElement('Deploy branch', record.deployBranch, `https://repo.it.auth.gr/app/universis-deploy/-/tree/${record.deployBranch}`));
    list.appendChild(createLiElement('Pipeline', record.pipeline, `https://repo.it.auth.gr/app/universis-deploy/pipelines/${record.pipeline}`));
    list.appendChild(createLiElement('Job', record.job, `https://repo.it.auth.gr/app/universis-deploy/-/jobs/${record.job}`));
    list.appendChild(createLiElement('User', record.user, `https://repo.it.auth.gr/${record.user}`));
    list.appendChild(createLiElement('Date', new Date(record.createdAt).toString()));

  }

  function showActionData(event) {
    const element = event.target;
    const params = {
      service: element.attributes['data-service'],
      pipeline: element.attributes['data-pipeline'],
      job: element.attributes['data-job']
    }

    getSingleActionData(params);
  }

  async function getSingleActionData(params) {
    document.querySelector('.js-error-alert').setAttribute("hidden", "hidden");
    const query = Object.entries(params).map((pair) => {
      return `${pair[0]}=${pair[1].value}`;
    })
    .join('&');
    let response;
    let formatted;
    try {
      response =  await fetch(`/log/history?${query}`);
      formatted = await response.json();
    } catch (err) {
      console.error(err);
      document.querySelector('.js-error-alert').innerHTML = "Could not fetch data using " + query;
      document.querySelector('.js-error-alert').removeAttribute("hidden");
    }

    let record;

    if (!formatted || (formatted && formatted.length === 0)) {
      // TODO: shoe err
    } else if (formatted && formatted.length > 1) {
      const sorted = formatted.sort((a, b) =>
        (new Date(b.createdAt).getTime()) - (new Date(a.createdAt).getTime())
      );

      record = sorted[0];
    } else {
      record = formatted[0];
    }

    showAttributeList(record);
  }

  async function fetchCategorizedData() {
    document.querySelector('.js-error-alert').setAttribute("hidden", "hidden");
    try {
      const response = await fetch('/log');
      const formatted = await response.json();

      const responsePerVps = await fetch('log/perVps');
      const formattedPerVps = await responsePerVps.json();

      const supportedServices = ['api-universis', 'api-registrar', 'registrar', 'teachers', 'students', 'register', 'qa-dashboard','dining','user-admin','evaluation','robot','robot3','spm-dashboard']
      document.querySelectorAll('.js-services-table-body tr').forEach(tr => tr.innerHTML = '')
      populateDescriptionRow(formattedPerVps)
      supportedServices.forEach((service) => {
        populateServiceRow(service, formatted[service]);
      });
    } catch (err) {
      console.error(err);
      document.querySelector('.js-error-alert')
        .innerHTML = "An error occurred. See the console for more information";
      document.querySelector('.js-error-alert').removeAttribute("hidden");
    }

  }

  function initialize() {
    fetchCategorizedData();
  }

  window.addEventListener('load', initialize());
}());
