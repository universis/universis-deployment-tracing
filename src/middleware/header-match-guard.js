const HeaderMatchGuard = (req, res, next) => {
  const serverToken = process.env.UNIVERSIS_DEPLOY_TRACING_AUTH_TOKEN;
  
  if  (!req.headers['x-universis-deploy-tracing-token'] || req.headers['x-universis-deploy-tracing-token'] !== serverToken) {
    res.status(401);
    res.json({
      status: 'err',
      err: 'Unauthorized'
    });
    return res.end;
  } else {
    next();
  }
  
}

module.exports = {
  HeaderMatchGuard
}