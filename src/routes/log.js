const express = require('express');
const router = express.Router();
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const { DB_KEY } = require('../constants');
const adapter = new FileSync(process.env.UNIVERSIS_DEPLOY_TRACING_DB_PATH);
const db = low(adapter);
const _ = require('lodash');
const { HeaderMatchGuard } = require('./../middleware/header-match-guard');

const defaultDb = {};
defaultDb[DB_KEY] = [];
db.defaults(defaultDb).write();


router.get('/history', (req, res, next) => {
  const response = db.get(DB_KEY).filter(req.query);
  res.json(response);
});

router.get('/perVps', (req, res, next) => {

  const response = {};

  const serviceRecordsRaw = db.get(DB_KEY).groupBy('vps');

  for (let [vpsName, vpsRecords] of Object.entries(serviceRecordsRaw.value())) {
    const serviceRecordsInServices = _.groupBy(vpsRecords, 'service');
    response[vpsName] = {};

    for ( let [serviceName, serviceRecords] of Object.entries(serviceRecordsInServices)) {
      const sorted = [...serviceRecords].sort((a, b) =>  {
        return (new Date(b.createdAt).getTime()) - (new Date(a.createdAt).getTime());
      });

      const mostRecent = sorted[0];
      response[vpsName][serviceName] = mostRecent;
    }

  }

  res.json(response);
});

router.get('/', (req, res, next) => {

  const response = {};

  const serviceRecordsRaw = db.get(DB_KEY).groupBy('service');

  for (let [serviceName, serviceRecords] of Object.entries(serviceRecordsRaw.value())) {
    const vpsRecordsInServices = _.groupBy(serviceRecords, 'vps');
    response[serviceName] = {};

    for ( let [vpsName, vpsRecords] of Object.entries(vpsRecordsInServices)) {
      const sorted = [...vpsRecords].sort((a, b) =>  {
        return (new Date(b.createdAt).getTime()) - (new Date(a.createdAt).getTime());
      });

      const mostRecent = sorted[0];
      response[serviceName][vpsName] = mostRecent;
    }

  }

  res.json(response);
});

/**
 *
 * @api {post} /log/service-deploy Add a service deploy
 * @apiName postDeployment
 * @apiGroup Log
 * @apiVersion 0.1.0
 *
 * @apiHeader  {String} Content-Type Content-Type value
 * @apiHeader  {String} x-universis-deploy-tracing-token Authorization token
 *
 * @apiParam (Body) {string} sourceCommit The commit hash of the deployed code
 * @apiParam (Body) {string} sourceBranch The branch of the deployed service
 * @apiParam (Body) {string} deployCommit The commit hash of the deploy script
 * @apiParam (Body) {string} deploymentBranch The branch at the repo.it.auth.gr of the deployment
 * @apiParam (Body) {string="test01", "test02", "test05", "test06", "staging", "production"} vps The VM that this job targets
 * @apiParam (Body) {string="api-universis", "api-registrar", "registrar", "teachers", "students", "register", "dining","user-admin","evaluation", "qa-dashboard","robot","robot3","spm-dashboard"} service The service this job targets
 * @apiParam (Body) {string} [description] The current status of the VM
 * @apiParam (Body) {string} job The job id that produced this deployment
 * @apiParam (Body) {string} pipeline The pipeline that produced this deployment
 * @apiParam (Body) {string} user The user who produced this deployment
 *
 * @apiSuccess {object} status Just an ok
 *
 */
router.post('/service-deploy', HeaderMatchGuard, (req, res, next) => {
  try {
    const request = {
      service,
      vps,
      job,
      pipeline,
      user,
      sourceCommit,
      sourceBranch,
      deployCommit,
      deployBranch
    } = req.body;
    const deployment = {}
    Object.keys(request)
      .forEach((key) => deployment[key] = req.sanitize(request[key]));

    deployment.type = 'serviceDeploy';
    deployment.createdAt = new Date();

    db.get(DB_KEY).push(deployment).write();

    res.json({
      status: 'ok'
    });
  } catch (err) {
    res.status(500);
    res.json({
      status: 'err',
      err: err.message
    });
  }
});

module.exports = router;
